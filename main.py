from tws_actions import TWSActions
from strategies import TradingStrategies
from settings import logger
from IBWrapper import contract
import time
import pandas as pd


if __name__ == "__main__":
    # Instantiate action class for TWS platform
    actions = TWSActions()

    # Instantiate stragies class
    strategy = TradingStrategies()

    # Get wrapper and socket client
    callback, tws = actions.instantiate_client_socket()

    # Connect to TWS
    actions._connect(tws)

    while 1:
        create = contract()  # Instantiate contract class

        # Get historical data
        data = actions.get_historical(tws, callback, 'JPM')

        short_SMA, long_SMA = strategy.SMA(data=data, short_length=5, long_length=10)

        logger.info("short_SMA {}, long_SMA {}".format(short_SMA, long_SMA))

        if short_SMA > long_SMA:
            pos = strategy.check_existing_pos()
            print(pos)
            if not pos:

                #Place order
                actions.send_order(callback=callback, tws=tws, ticker_symbol='JPM', order_type='MKT', qty=10, order_action='BUY')

                time.sleep(2)
                order_status_update = pd.DataFrame(callback.order_Status,
                                                   columns=['orderId', 'status', 'filled', 'remaining', 'avgFillPrice',
                                                            'permId', 'parentId', 'lastFillPrice', 'clientId',
                                                            'whyHeld'])
            # if the order is filled, it should be picked up by the
            # check_existing_pos function. The place order should not
            # be fired.
