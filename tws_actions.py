import pandas as pd
import time
from datetime import datetime
from IBWrapper import IBWrapper, contract
from ib.ext.EClientSocket import EClientSocket
from settings import logger


class TWSActions:
    def __init__(self):
        self._accountName = "DU000000"
        self._host = ""
        self._port = 7497
        self._clientId = 1001
        self._order_id = 1001

    @staticmethod
    def instantiate_client_socket():
        '''
        Instantiates IbWrapper and EClientSocket to return data to callback
        :return: EClientSocket and Callback
        '''

        callback = IBWrapper()  # Instantiate IBWrapper. callback
        tws = EClientSocket(callback)

        return callback, tws

    def _connect(self, tws):
        '''
        Connect to tws client socket
        :return: None
        '''

        # Connect to TWS
        logger.info("CONNECTING TO TWS...")
        tws.eConnect(self._host, self._port, self._clientId)

    @staticmethod
    def _disconnect(tws):
        '''
        Disconnect from client socket
        :return:
        '''
        logger.info("DISCONNECTING FROM TWS...")
        tws.eDisconnect()

    def send_order(self, callback, tws, ticker_symbol, order_type, qty, order_action):
        '''
        Sends market and limit orders to TWS
        :param callback: callback to retrieve data
        :param tws: socket client
        :param ticker_symbol: Stock symbol
        :param order_type: MKT, LIMIT, etc.
        :param qty: Order quantity
        :param order_action: BUY, SELL, etc
        :return:
        '''

        # Orders #############################################################################
        # placeOrder           --->   orderStatus**         self.order_Status
        # cancelorder
        #                      --->   openOrderEnd          self.open_OrderEnd_flag
        # reqOpenOrders        --->   openOrder*            self.open_Order
        #                      --->   orderStatus**
        # reqAllOpenOrders     --->   openOrder*
        #                      --->   orderStatus**
        # reqAutoOpenOrders    --->   openOrder*
        #                      --->   orderStatus**
        # reqIds               --->   nextValidId           self.next_ValidId
        #                      --->   deltaNeutralValidation
        # exerciseOptions
        # reqGlobalCancel
        ###################################################################################

        logger.info("PLACING ORDER FOR {}".format(ticker_symbol))

        # Place order through TWS
        create = contract()

        # Initiate attributes to receive data. At some point we need a separate class for this
        callback.initiate_variables()

        tws.reqIds(1)  # Need to request next valid order Id
        time.sleep(2)  # wait for response from server
        order_id = callback.next_ValidId
        contract_info1 = create.create_contract(symbol=ticker_symbol, secType='STK', exchange='SMART', currency='USD')
        order_info1 = create.create_order(account=self._accountName, orderType=order_type, totalQuantity=qty,
                                          action=order_action)
        tws.placeOrder(id=order_id, contract=contract_info1, order=order_info1)

    @staticmethod
    def get_historical(tws, callback, ticker_symbol):
        '''
        Requests historical data with hard coded look back period from today

        :param tws: Client Esocket (Connection)
        :param callback: IBWrapper Instance
        :param ticker_symbol: stock ticker symbol
        :return: Historical data as Pandas dataframe
        '''

        tickerId = 1000
        data_endtime = datetime.now().strftime("%Y%m%d %H:%M:%S")

        create = contract()
        callback.initiate_variables()
        contract_Details = create.create_contract(symbol=ticker_symbol, secType='STK', exchange='SMART', currency='USD')

        tws.reqHistoricalData(tickerId=tickerId,
                              contract=contract_Details,
                              endDateTime=data_endtime,
                              durationStr="1 D",
                              barSizeSetting="1 min",
                              whatToShow="TRADES",
                              useRTH=0,
                              formatDate=1)
        time.sleep(2)
        data = pd.DataFrame(callback.historical_Data,
                            columns=["reqId", "date", "open",
                                     "high", "low", "close",
                                     "volume", "count", "WAP",
                                     "hasGaps"])

        data = data.iloc[:-1]

        return data
