from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
import datetime
#import Report
import pandas_model
import pandas as pd
import functools


class Ui_MainWindow(QWidget):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1460, 710)

        # Disable Maximize Button
        MainWindow.setWindowFlags(MainWindow.windowFlags() & ~QtCore.Qt.WindowMaximizeButtonHint)

        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("ReportCardsIcon.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tableView = QtWidgets.QTableView(self.centralwidget)
        self.tableView.setGeometry(QtCore.QRect(10, 340, 1440, 331))
        self.tableView.setObjectName("tableView")
        self.tableView.setSortingEnabled(True)
        self.tableView.setAlternatingRowColors(True)
        self.proxy = QtCore.QSortFilterProxyModel(self)
        self.regexGroupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.regexGroupBox.setGeometry(QtCore.QRect(10, 250, 191, 80))
        self.regexGroupBox.setObjectName("regexGroupBox")
        self.formLayoutWidgetRegex = QtWidgets.QWidget(self.regexGroupBox)
        self.formLayoutWidgetRegex.setGeometry(QtCore.QRect(10, 20, 160, 48))
        self.formLayoutWidgetRegex.setObjectName("formLayoutWidgetRegex")
        self.formLayoutRegex = QtWidgets.QFormLayout(self.formLayoutWidgetRegex)
        self.formLayoutRegex.setContentsMargins(0, 0, 0, 0)
        self.formLayoutRegex.setObjectName("formLayoutRegex")
        self.lineEditRegex = QtWidgets.QLineEdit(self.formLayoutWidgetRegex)
        self.lineEditRegex.setObjectName("lineEditRegex")
        self.formLayoutRegex.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.lineEditRegex)
        self.comboBoxRegex = QtWidgets.QComboBox(self.formLayoutWidgetRegex)
        self.comboBoxRegex.setObjectName("comboBoxRegex")
        self.formLayoutRegex.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.comboBoxRegex)
        self.label_regex = QtWidgets.QLabel(self.formLayoutWidgetRegex)
        self.label_regex.setObjectName("label_regex")
        self.formLayoutRegex.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_regex)
        self.label_regex2 = QtWidgets.QLabel(self.formLayoutWidgetRegex)
        self.label_regex2.setObjectName("label_regex2")
        self.formLayoutRegex.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_regex2)
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(280, 20, 151, 211))
        self.listWidget.setObjectName("listWidget")
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 211, 60))
        self.groupBox.setObjectName("groupBox")
        self.formLayoutWidget = QtWidgets.QWidget(self.groupBox)
        self.formLayoutWidget.setGeometry(QtCore.QRect(10, 20, 191, 51))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.formLayoutWidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)

        #Trading strategy combo box
        self.strategySelectComboBox = QtWidgets.QComboBox(self.formLayoutWidget)
        self.strategySelectComboBox.setObjectName("strategySelectComboBox")
        self.strategySelectComboBox.addItem("")
        self.strategySelectComboBox.addItem("")
        self.strategySelectComboBox.addItem("")
        self.strategySelectComboBox.addItem("")

        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.strategySelectComboBox)
        self.label_2 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)

        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 110, 251, 121))
        self.groupBox_2.setObjectName("groupBox")
        self.gridLayoutWidget = QtWidgets.QWidget(self.groupBox_2)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 20, 231, 81))
        self.gridLayoutWidget.setObjectName("portfolioGridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.label_5 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 1, 0, 1, 1)
        self.toolButton = QtWidgets.QToolButton(self.gridLayoutWidget)
        self.toolButton.setObjectName("toolButton")
        self.gridLayout.addWidget(self.toolButton, 0, 2, 1, 1)
        self.dateEdit = QtWidgets.QDateEdit(self.gridLayoutWidget)
        self.dateEdit.setObjectName("dateEdit")
        self.gridLayout.addWidget(self.dateEdit, 0, 1, 1, 1)
        self.toolButton_2 = QtWidgets.QToolButton(self.gridLayoutWidget)
        self.toolButton_2.setObjectName("toolButton_2")
        self.toolButton.clicked.connect(functools.partial(self.showCalendarDialog, "Start"))
        self.toolButton_2.clicked.connect(functools.partial(self.showCalendarDialog, "End"))

        self.gridLayout.addWidget(self.toolButton_2, 1, 2, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 0, 0, 1, 1)
        self.dateEdit_2 = QtWidgets.QDateEdit(self.gridLayoutWidget)
        self.dateEdit_2.setObjectName("dateEdit_2")
        self.gridLayout.addWidget(self.dateEdit_2, 1, 1, 1, 1)
        self.pushButton = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 2, 0, 1, 1)
        self.pushButton_4 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton_4.setObjectName("pushButton4")
        self.gridLayout.addWidget(self.pushButton_4, 2, 1, 1, 1)

        self.groupBox_3 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_3.setGeometry(QtCore.QRect(450, 19, 101, 71))
        self.groupBox_3.setTitle("")
        self.groupBox_3.setObjectName("groupBox_3")
        self.pushButton_3 = QtWidgets.QPushButton(self.groupBox_3)
        self.pushButton_3.setGeometry(QtCore.QRect(10, 40, 81, 23))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_2 = QtWidgets.QPushButton(self.groupBox_3)
        self.pushButton_2.setGeometry(QtCore.QRect(10, 10, 81, 23))
        self.pushButton_2.setObjectName("pushButton_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1335, 21))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuExport_As = QtWidgets.QMenu(self.menuFile)
        self.menuExport_As.setObjectName("menuExport_As")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        self.menuEdit = QtWidgets.QMenu(self.menubar)
        self.menuEdit.setObjectName("menuEdit")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionEstablishing_an_ODBC_Connection = QtWidgets.QAction(MainWindow)
        self.actionEstablishing_an_ODBC_Connection.setObjectName("actionEstablishing_an_ODBC_Connection")
        self.actionOpen = QtWidgets.QAction(MainWindow)
        self.actionOpen.setObjectName("actionOpen")
        self.actionExit = QtWidgets.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.actionExit.triggered.connect(QApplication.quit)
        self.actionChange_Employee_Status = QtWidgets.QAction(MainWindow)
        self.actionChange_Employee_Status.setObjectName("actionChange_Employee_Status")
        self.actionPDF = QtWidgets.QAction(MainWindow)
        self.actionPDF.setObjectName("actionPDF")
        self.actionText_File = QtWidgets.QAction(MainWindow)
        self.actionText_File.setObjectName("actionText_File")
        self.menuExport_As.addAction(self.actionPDF)
        self.menuFile.addAction(self.actionOpen)
        self.menuFile.addAction(self.menuExport_As.menuAction())
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExit)
        self.menuHelp.addAction(self.actionEstablishing_an_ODBC_Connection)
        self.menuEdit.addAction(self.actionChange_Employee_Status)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.actionPDF.triggered.connect(self.showReportDialaog)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


        #self.strategySelectComboBox.currentTextChanged.connect(self.on_pod_changed)


        self.lineEditRegex.textChanged.connect(self.on_lineEdit_textChanged)
        self.comboBoxRegex.currentIndexChanged.connect(self.on_comboBox_currentIndexChanged)

        self.dateEdit.setDate(QtCore.QDate(2018, 1, 1))
        self.dateEdit_2.setDate(QtCore.QDate(2018, datetime.datetime.today().month, 1))

        self.pushButton.clicked.connect(self.load_table)

        self.pushButton_2.clicked.connect(self.add_employee)
        self.pushButton_3.clicked.connect(self.edit_employee)
        self.pushButton_4.clicked.connect(self.resetForm)

        self.listWidget.setAlternatingRowColors(True)
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.group_dict = {"Cardiology Pod": ["Cardiology"], "Diagnostics Pod": ["Diagnostics"],
                           "HA Scheduling Pod": ["Health Alliance"],
                           "MARL Scheduling Pod": ["Marlborough"], "MSK Pod": ["Musculoskeletal"],
                           'MSKConcierge Pod': ['Musculoskeletal'],
                           "Pediatrics Pod": ["Pediatrics"], "Primary Care Pod": ["Primary Care"],
                           "Specialty Pod": ["Specialty"], "Surgical Pod": ["Surgical"], "Women's Pod": ["Womens"],
                           "855-UMass-MD": ["Access"], "Financial Clearance": ['RMD', 'Pre Registration'],
                           "Central Scheduling": ['Cardiology', 'Diagnostics', 'Pediatrics', 'Musculoskeletal',
                                                  'Primary Care', 'Specialty', 'Surgical', 'Womens']}

        self.updateMsgEmployee = QMessageBox()
        self.updateMsgEmployee.setIcon(QMessageBox.Question)
        self.updateMsgEmployee.setText("Are you sure you want to update \n this employee's information?")
        self.updateMsgEmployee.setWindowTitle("Warning")
        self.updateMsgEmployee.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))
        self.updateMsgEmployee.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        self.updateMsgEmployee.setDefaultButton(QMessageBox.No)

        self.msgGroup = QMessageBox()
        self.msgGroup.setIcon(QMessageBox.Information)
        self.msgGroup.setText("No Group Detected!")
        self.msgGroup.setWindowTitle("No Group")
        self.msgGroup.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))

        self.msgDownload = QMessageBox()
        self.msgDownload.setIcon(QMessageBox.Information)
        self.msgDownload.setText("Data is loaded!")
        self.msgDownload.setWindowTitle("Download Complete")
        self.msgDownload.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))

        self.msgEmployee = QMessageBox()
        self.msgEmployee.setIcon(QMessageBox.Information)
        self.msgEmployee.setText("No Employees Detected!")
        self.msgEmployee.setWindowTitle("No Employees")
        self.msgEmployee.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))

        self.msgEmployee2 = QMessageBox()
        self.msgEmployee2.setIcon(QMessageBox.Information)
        self.msgEmployee2.setText("Please Select a Single Employee!")
        self.msgEmployee2.setWindowTitle("Too Many Employees Selected")
        self.msgEmployee2.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))

        self.removeMsg = QMessageBox()
        self.removeMsg.setIcon(QMessageBox.Question)
        self.removeMsg.setText("Are you sure you want to remove \n the employee(s) from this group?")
        self.removeMsg.setWindowTitle("Warning")
        self.removeMsg.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))
        self.removeMsg.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        self.removeMsg.setDefaultButton(QMessageBox.No)

        self.fmtMsg = QMessageBox()
        self.fmtMsg.setIcon(QMessageBox.Information)
        self.fmtMsg.setText("Please Enter the Employee Name as: \n  First Name, Last Name")
        self.fmtMsg.setWindowTitle("Invalid Employee Name Format")
        self.fmtMsg.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))
        self.fmtMsg.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        self.fmtMsg.setDefaultButton(QMessageBox.No)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "IB Automated Trader"))

        __sortingEnabled = self.listWidget.isSortingEnabled()
        self.listWidget.setSortingEnabled(False)
        self.listWidget.setSortingEnabled(__sortingEnabled)
        self.groupBox.setTitle(_translate("MainWindow", "Strategy Selector:"))

        self.strategySelectComboBox.setItemText(0, _translate("MainWindow", ""))
        self.strategySelectComboBox.setItemText(1, _translate("MainWindow", "Simple Moving Average (SMA)"))
        self.strategySelectComboBox.setItemText(2, _translate("MainWindow", "MACD"))
        self.strategySelectComboBox.setItemText(3, _translate("MainWindow", "PSAR"))

        self.groupBox_2.setTitle(_translate("MainWindow", "Date Range:"))
        self.label_5.setText(_translate("MainWindow", "End Date:"))
        self.pushButton.setText(_translate("MainWindow", "Submit"))
        self.pushButton_4.setText(_translate("MainWindow", "Reset Form"))
        self.label_3.setText(_translate("MainWindow", "Start Date:"))
        self.pushButton_3.setText(_translate("MainWindow", "Edit"))
        self.pushButton_2.setText(_translate("MainWindow", "Add"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuExport_As.setTitle(_translate("MainWindow", "Export As"))
        self.menuHelp.setTitle(_translate("MainWindow", "Help"))
        self.menuEdit.setTitle(_translate("MainWindow", "Edit"))
        self.actionEstablishing_an_ODBC_Connection.setText(_translate("MainWindow", "ODBC Connection"))
        self.actionOpen.setText(_translate("MainWindow", "Open"))
        self.actionExit.setText(_translate("MainWindow", "Exit"))
        self.actionChange_Employee_Status.setText(_translate("MainWindow", "Change Employee Status"))
        self.actionPDF.setText(_translate("MainWindow", "PDF"))
        self.regexGroupBox.setTitle(_translate("MainWindow", "Table View Filters"))
        self.label_regex.setText(_translate("MainWindow", "Column:"))
        self.label_regex2.setText(_translate("MainWindow", "Value:"))
        self.toolButton.setText(_translate("MainWindow", "..."))
        self.toolButton_2.setText(_translate("MainWindow", "..."))

    def add_employee(self):
        try:
            if self.comboBox.currentIndex() == 0:
                self.msgGroup.exec_()
            else:
                self.showAddDialog()

        except Exception as e:
            print(str(e))

    def edit_employee(self):
        self.listItems = ([item.text() for item in self.listWidget.selectedItems()])

        try:
            if self.comboBox.currentIndex() == 0:
                self.msgGroup.exec_()
            elif len(self.listItems) == 0:
                self.msgEmployee.exec_()
            elif len(self.listItems) > 1:
                self.msgEmployee2.exec_()
            else:
                self.showEditDialog()

        except Exception as e:
            print(str(e))

    def showAddDialog(self):
        self.d = QDialog()

        self.createFormGroupBoxAdd()

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.acceptBtn)
        buttonBox.rejected.connect(self.d.reject)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.formGroupBox)
        mainLayout.addWidget(buttonBox)
        self.d.setLayout(mainLayout)

        self.d.setWindowTitle("Employee Menu")
        self.d.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))
        self.d.exec_()

    def showEditDialog(self):
        self.d = QDialog()

        self.createFormGroupBoxEdit()

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(self.updateEmployeeSQL)
        buttonBox.rejected.connect(self.d.reject)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.formGroupBox)
        mainLayout.addWidget(buttonBox)
        self.d.setLayout(mainLayout)

        self.d.setWindowTitle("Edit Employee Menu")
        self.d.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))
        self.d.exec_()

    def createFormGroupBoxAdd(self):
        self.formGroupBox = QGroupBox("Add Employee")
        layout = QFormLayout()

        self.qlineedit1 = QLineEdit()
        self.qlineedit1.setPlaceholderText("Last Name, First Name")
        layout.addRow(QLabel("Name:"), self.qlineedit1)
        self.formGroupBox.setLayout(layout)

    def createFormGroupBoxEdit(self):
        self.formGroupBox = QGroupBox("Edit Employee")
        layout = QFormLayout()

        self.qlineeditEdit = QLineEdit()
        self.qlineeditEdit.setText(self.listItems[0])
        self.comboBoxEdit_2 = QtWidgets.QComboBox()
        self.comboBoxEdit_2.addItem("")
        self.comboBoxEdit_2.addItem("")
        self.comboBoxEdit_2.setItemText(0, "Yes")
        self.comboBoxEdit_2.setItemText(1, "No")

        self.comboBoxEdit = QtWidgets.QComboBox()
        self.comboBoxEdit.addItem("")
        self.comboBoxEdit.addItem("")
        self.comboBoxEdit.addItem("")
        self.comboBoxEdit.addItem("")
        self.comboBoxEdit.addItem("")
        self.comboBoxEdit.addItem("")
        self.comboBoxEdit.addItem("")
        self.comboBoxEdit.addItem("")
        self.comboBoxEdit.addItem("")
        self.comboBoxEdit.addItem("")
        self.comboBoxEdit.addItem("")
        self.comboBoxEdit.addItem("")

        self.comboBoxEdit.setItemText(0, "Cardiology Pod")
        self.comboBoxEdit.setItemText(1, "Diagnostics Pod")
        self.comboBoxEdit.setItemText(2, "HA_Scheduling Pod")
        self.comboBoxEdit.setItemText(3, "MARL_Scheduling Pod")
        self.comboBoxEdit.setItemText(4, "Musculoskeletal Pod")
        self.comboBoxEdit.setItemText(5, "MSK Concierge Pod")
        self.comboBoxEdit.setItemText(6, "Pediatrics Pod")
        self.comboBoxEdit.setItemText(7, "Primary Care Pod")
        self.comboBoxEdit.setItemText(8, "Specialty Pod")
        self.comboBoxEdit.setItemText(9, "Surgical Pod")
        self.comboBoxEdit.setItemText(10, "Womens Pod")
        self.comboBoxEdit.setItemText(11, "Access")

        self.comboBoxEdit.setCurrentIndex(self.comboBox.currentIndex() - 1)

        layout.addRow(QLabel("Name:"), self.qlineeditEdit)
        layout.addRow(QLabel("Active:"), self.comboBoxEdit_2)
        layout.addRow(QLabel("Team:"), self.comboBoxEdit)

        self.formGroupBox.setLayout(layout)

    def remove_employee(self):
        if self.listWidget.count() == 0:
            self.msgEmployee.exec_()
        else:
            try:
                msg = self.removeMsg.exec_()
                if msg == QMessageBox.Yes:
                    listItems = ([item.text() for item in self.listWidget.selectedItems()])

                    # Set Employee to Inactive in DB
                    sql = """UPDATE [PAS_Scheduling].[dbo].[LT Report Cards Employees] SET [Active] = ? WHERE [Employee Name] = ? and [Team] = ?"""
                    self.cursor.execute(sql, "No", str(self.qlineeditEdit.text()),
                                        str(self.comboBoxEdit.currentText().replace("Pod", "")))
                    self.conn.commit()

                    for selectedItem in self.listWidget.selectedItems():
                        self.listWidget.takeItem(self.listWidget.row(selectedItem))

            except Exception as e:
                print(str(e))

    def showReportDialaog(self):
        try:
            d = Dialog(self)
            d.show()
        except Exception as e:
            print(str(e))

    def acceptCal(self, se):
        date = self.calendarWidget.selectedDate()

        if se == "Start":
            self.dateEdit.setDate(date)
        else:
            self.dateEdit_2.setDate(date)

        self.d.close()

    def showCalendarDialog(self, se):
        try:
            print(se)
            self.d = QDialog()

            self.d.resize(276, 247)
            self.calendarWidget = QtWidgets.QCalendarWidget(self.d)
            self.calendarWidget.setGeometry(QtCore.QRect(10, 10, 256, 183))
            self.calendarWidget.setObjectName("calendarWidget")
            self.buttonBox = QtWidgets.QDialogButtonBox(self.d)
            self.buttonBox.setGeometry(QtCore.QRect(10, 210, 156, 23))
            self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
            self.buttonBox.accepted.connect(functools.partial(self.acceptCal, se))
            self.buttonBox.rejected.connect(self.d.reject)
            self.buttonBox.setObjectName("buttonBox")

            self.d.setWindowTitle("Employee Menu")
            self.d.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))
            self.d.exec_()

        except Exception as e:
            print(str(e))

    def on_pod_changed(self):
        self.comboBox_2.setCurrentIndex(0)

        if self.comboBox.currentIndex() == 0:
            self.listWidget.clear()
        else:
            self.group_value = self.group_dict.get(str(self.comboBox.currentText()))

            # Query data
            sql = '''SELECT [Employee Name] from [PAS_Scheduling].[dbo].[LT Report Cards Employees] WHERE Team IN ('%s') AND Active = ? ORDER BY [Employee Name] ASC''' % (
                "','".join(self.group_value))
            self.cursor.execute(sql, ('Yes'))
            self.get_employee_data()

    def on_group_changed(self):
        self.comboBox.setCurrentIndex(0)

        if self.comboBox_2.currentIndex() == 0:
            self.listWidget.clear()

        else:
            self.group_value = self.group_dict.get(self.comboBox_2.currentText())

            sql = '''SELECT [Employee Name] from [PAS_Scheduling].[dbo].[LT Report Cards Employees] WHERE Team IN ('%s') AND Active = ? ORDER BY [Employee Name] ASC''' % (
                "','".join(self.group_value))
            self.cursor.execute(sql, ('Yes'))
            self.get_employee_data()

    def on_lineEdit_textChanged(self, text):
        search = QtCore.QRegExp(text,
                                QtCore.Qt.CaseInsensitive,
                                QtCore.QRegExp.RegExp
                                )

        self.proxy.setFilterRegExp(search)

    def on_comboBox_currentIndexChanged(self, index):
        self.proxy.setFilterKeyColumn(index)

    def get_employee_data(self):
        # Get Data
        data = self.cursor.fetchall()
        self.listWidget.clear()

        _translate = QtCore.QCoreApplication.translate
        __sortingEnabled = self.listWidget.isSortingEnabled()
        self.listWidget.setSortingEnabled(False)

        for x, row in enumerate(data, 0):
            item = QtWidgets.QListWidgetItem()
            self.listWidget.addItem(item)

            item = self.listWidget.item(x)
            item.setText(_translate("MainWindow", row[0]))

        self.listWidget.setSortingEnabled(__sortingEnabled)

    def load_table(self):
        try:

            if self.comboBox.currentIndex() == 0 and self.comboBox_2.currentIndex() == 0:
                self.msgGroup.exec_()

            else:
                sql = """
                        SELECT MonthStartDate as [Month],
                        AgentName,
                        [Staff Time]/3600 as [Logged In Hours],
                        [ACD Time]/3600 AS [Talk Time Hours],
                        [AUX Total]/CAST(3600 AS FLOAT)  AS [Total Aux Hours],
                        ([AUX Total]-[Aux0]-[Aux1]-[Aux11])/3600 AS [Not Ready Hours],
                        CASE
                            WHEN [Staff Time] = 0 THEN 0
                            ELSE (([AUX Total]-[Aux0]-[Aux1]-[Aux11]-[Aux16])/CAST(([Staff Time]-[Aux1]-[Aux11]-[Aux16])AS FLOAT))
                        END AS [Not Ready Percentage],
                        [ACD Calls] AS [Calls Answered],
                        [RONA] AS [Requeued Calls],
                        CASE
                            WHEN [ACD Calls] = 0 THEN 0
                            ELSE ([RONA]/CAST([ACD Calls] AS FLOAT))
                        END AS [Requeued Call Percentage],
                        [Transferred Calls] as [Transferred],
                        CASE
                            WHEN [ACD Calls] = 0 THEN 0
                            ELSE ([Transferred Calls]/CAST([ACD Calls] AS FLOAT))
                        END AS [Transferred Percentage],
                        CASE
                            WHEN [ErrorCnt] IS NULL THEN 0
                            ELSE [ErrorCnt]
                        END AS [Errors],
                        CASE
                            WHEN [ACD Calls] = 0 THEN 0
                            ELSE CAST([ACD Time] AS FLOAT)/[ACD Calls]/60
                        END AS [Average Talk Time]

                      FROM [PAS_Scheduling].[dbo].ReportCards2

                      WHERE [Team] IN ('%s') AND 
                      [Team] != ? AND 
                      [MonthStartDate] >= ? AND 
                      [MonthStartDate] <= ?
                      ORDER BY [AgentName], [MonthStartDate] """ % ("','".join(self.group_value))

                self.start_date = datetime.datetime.strptime(self.dateEdit.text(), "%m/%d/%Y").strftime("%Y-%m-%d")
                self.end_date = datetime.datetime.strptime(self.dateEdit_2.text(), "%m/%d/%Y").strftime("%Y-%m-%d")

                df = pd.read_sql_query(sql, self.conn,
                                       params=('Work From Home', str(self.start_date), str(self.end_date)))

                df['Not Ready Percentage'] = df.apply(lambda x: "{0:.2f}%".format(x['Not Ready Percentage'] * 100),
                                                      axis=1)
                df['Requeued Call Percentage'] = df.apply(
                    lambda x: "{0:.2f}%".format(x['Requeued Call Percentage'] * 100), axis=1)
                df['Transferred Percentage'] = df.apply(lambda x: "{0:.2f}%".format(x['Transferred Percentage'] * 100),
                                                        axis=1)
                df['Average Talk Time'] = df.apply(lambda x: "{0:.2f}".format(x['Average Talk Time']), axis=1)
                df['Total Aux Hours'] = df.apply(lambda x: "{0:.2f}".format(x['Total Aux Hours']), axis=1)

                try:

                    cols = ['Month', 'Agent \n Name', 'Logged In \nHours', 'Talk Time \nHours', 'Total Aux \nHours',
                            'Not Ready \nHours', 'Not Ready % \n\n 8.0%', 'Calls \nAnswered', 'Requeued \nCalls',
                            'Requeued \nCall % \n\n 0.6%',
                            'Transferred', 'Transfer %', 'Errors', 'Average \nTalk Time \n\n 5']
                    df.columns = cols

                except Exception as e:
                    print(str(e))
                model = pandas_model.PandasModel(df)
                self.proxy.setSourceModel(model)

                l = ["Month", "Employee"]
                self.comboBoxRegex.addItems(l)
                self.tableView.setModel(self.proxy)

                self.msgDownload.exec_()
        except Exception as e:
            print(str(e))

    def resetForm(self):
        self.listWidget.clear()
        self.comboBox.setCurrentIndex(0)
        self.comboBox_2.setCurrentIndex(0)
        self.lineEditRegex.clear()
        self.dateEdit.setDate(QtCore.QDate(2018, 1, 1))
        self.dateEdit_2.setDate(QtCore.QDate(2018, datetime.datetime.today().month, 1))


class Ui_Dialog(QWidget):
    def setupUi(self, Dialog, parent):
        self.parent = parent
        Dialog.resize(460, 502)
        Dialog.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))
        Dialog.setStyleSheet("font: 8pt \"MS Shell Dlg 2\";")
        self.gridLayoutWidget = QtWidgets.QWidget(Dialog)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 433, 481))
        self.gridLayoutWidget.setObjectName("portfolioGridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.toolButton = QtWidgets.QToolButton(self.gridLayoutWidget)
        self.toolButton.setObjectName("toolButton")
        self.gridLayout.addWidget(self.toolButton, 6, 3, 1, 1)
        self.label = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 8, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 4, 1, 1, 2)
        self.pushButton_2 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.gridLayout.addWidget(self.pushButton_2, 10, 1, 1, 1)
        self.listWidget = QtWidgets.QListWidget(self.gridLayoutWidget)
        self.listWidget.setObjectName("listWidget")
        self.gridLayout.addWidget(self.listWidget, 9, 1, 1, 1)
        self.pushButton = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 11, 1, 1, 1)
        self.dateEdit = QtWidgets.QDateEdit(self.gridLayoutWidget)
        self.dateEdit.setObjectName("dateEdit")
        self.gridLayout.addWidget(self.dateEdit, 6, 2, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 12, 1, 1, 2)
        self.label_3 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 1, 1, 1, 1)
        self.comboBoxDialog = QtWidgets.QComboBox(self.gridLayoutWidget)
        self.comboBoxDialog.setObjectName("comboBoxDialog")
        self.gridLayout.addWidget(self.comboBoxDialog, 2, 1, 1, 1)
        self.dateEdit_2 = QtWidgets.QDateEdit(self.gridLayoutWidget)
        self.dateEdit_2.setObjectName("dateEdit_2")
        self.gridLayout.addWidget(self.dateEdit_2, 7, 2, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 6, 1, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 7, 1, 1, 1)
        self.toolButton_2 = QtWidgets.QToolButton(self.gridLayoutWidget)
        self.toolButton_2.setObjectName("toolButton_2")
        self.gridLayout.addWidget(self.toolButton_2, 7, 3, 1, 1)
        self.pushButton_4 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton_4.setObjectName("pushButton_4")
        self.gridLayout.addWidget(self.pushButton_4, 14, 1, 1, 1)
        self.listWidget_2 = QtWidgets.QListWidget(self.gridLayoutWidget)
        self.listWidget_2.setObjectName("listWidget_2")
        self.gridLayout.addWidget(self.listWidget_2, 9, 2, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.gridLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 8, 2, 1, 1)
        self.pushButton_3 = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.pushButton_3.setObjectName("pushButton_3")
        self.gridLayout.addWidget(self.pushButton_3, 13, 1, 1, 1)
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.listWidget_2.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.listWidget.setAlternatingRowColors(True)
        self.listWidget_2.setAlternatingRowColors(True)
        self.comboBoxDialog.currentIndexChanged.connect(self.on_pod_changed)
        self.pushButton_2.clicked.connect(self.add_employee)
        self.pushButton.clicked.connect(self.remove_employee)
        self.pushButton_3.clicked.connect(self.saveAs)
        self.pushButton_4.clicked.connect(self.resetForm)
        self.toolButton.clicked.connect(functools.partial(self.showCalendarDialog, "Start"))
        self.toolButton_2.clicked.connect(functools.partial(self.showCalendarDialog, "End"))
        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Report Menu", "Report Menu"))
        self.label.setText(_translate("Dialog", "Active Employees:"))
        self.pushButton_2.setText(_translate("Dialog", "Add Employee"))
        self.pushButton.setText(_translate("Dialog", "Remove Employee"))
        self.pushButton_3.setText(_translate("Dialog", "Submit"))
        self.pushButton_4.setText(_translate("Dialog", "Reset Form"))
        self.label_4.setText(_translate("Dialog", "Start Date:"))
        self.label_5.setText(_translate("Dialog", "End Date:"))
        self.label_2.setText(_translate("Dialog", "Selected Employees:"))
        self.label_3.setText(_translate("Dialog", "Selected Pod/Group:"))
        self.toolButton.setText(_translate("Dialog", "..."))
        self.toolButton_2.setText(_translate("Dialog", "..."))

    def add_employee(self):
        listItems = ([item.text() for item in self.listWidget.selectedItems()])
        self.listWidget_2.addItems(listItems)

    def remove_employee(self):
        for SelectedItem in self.listWidget_2.selectedItems():
            self.listWidget_2.takeItem(self.listWidget_2.row(SelectedItem))

    def setFields(self):
        sd = datetime.datetime.strptime(str(self.parent.dateEdit.text()), '%m/%d/%Y')
        ed = datetime.datetime.strptime(str(self.parent.dateEdit_2.text()), '%m/%d/%Y')

        self.dateEdit.setDate(QtCore.QDate(sd.year, sd.month, sd.day))
        self.dateEdit_2.setDate(QtCore.QDate(ed.year, ed.month, ed.day))

        comboItems = [self.parent.comboBox.itemText(i) for i in range(self.parent.comboBox.count())]
        self.comboBoxDialog.addItems(comboItems)

        comboItems2 = [self.parent.comboBox_2.itemText(i) for i in range(self.parent.comboBox_2.count())]
        comboItems2.remove("")
        self.comboBoxDialog.addItems(comboItems2)

        listItems = [self.parent.listWidget.item(i).text() for i in range(0, self.parent.listWidget.count())]
        self.listWidget.addItems(listItems)

    def resetForm(self):
        self.listWidget.clear()
        self.listWidget_2.clear()
        self.comboBoxDialog.setCurrentIndex(0)
        self.dateEdit.setDate(QtCore.QDate(2018, 1, 1))
        self.dateEdit_2.setDate(QtCore.QDate(2018, datetime.datetime.today().month, 1))

    def on_pod_changed(self):
        if self.comboBoxDialog.currentIndex() == 0:
            self.listWidget.clear()
            self.listWidget_2.clear()

        else:
            self.group = self.parent.group_dict.get(str(self.comboBoxDialog.currentText()))

            # Query data
            sql = '''SELECT [Employee Name] from [PAS_Scheduling].[dbo].[LT Report Cards Employees] WHERE Team IN ('%s') AND Active = ?''' % (
                "','".join(self.group))
            self.parent.cursor.execute(sql, ('Yes'))

            data = self.parent.cursor.fetchall()
            result = [row[0] for row in data]

            self.listWidget.clear()
            self.listWidget.addItems(result)

    def saveAs(self):
        try:

            if self.listWidget_2.count() == 0:
                self.parent.msgEmployee.exec_()
            elif self.comboBoxDialog.currentIndex() == 0:
                self.parent.msgGroup.exec_()
            else:
                options = QFileDialog.Options()
                options |= QFileDialog.DontUseNativeDialog
                fileName, _ = QFileDialog.getSaveFileName(self, "QFileDialog.getSaveFileName()", "",
                                                          "PDF (*.pdf)", options=options)

                listItems = [self.listWidget_2.item(i).text() for i in range(0, self.listWidget_2.count())]

                if fileName:
                    pod = self.comboBoxDialog.currentText().replace(" Pod", "")
                    if pod == "Women's":
                        pod = "Womens"
                    elif pod == "MARL Scheduling":
                        pod = "Marlborough"
                    elif pod == "HA Scheduling":
                        pod = "Health Alliance"
                    elif pod == "MSK":
                        pod = "Musculoskeletal"
                    elif pod == "855-UMass-MD":
                        pod = "Access"

                    r = Report.rep(fname=fileName, pod=str(pod), agentGroup=listItems, startDate=str(
                        datetime.datetime.strptime(self.dateEdit.text(), "%m/%d/%Y").strftime("%Y-%m-%d")), endDate=str(
                        datetime.datetime.strptime(self.dateEdit_2.text(), "%m/%d/%Y").strftime("%Y-%m-%d")))
                    r.cover()
                    r.savePDF()

                    self.parent.msgDownload.exec_()
        except Exception as e:
            print(str(e))

    def acceptBtn(self, se):
        date = self.calendarWidget.selectedDate()

        if se == "Start":
            self.dateEdit.setDate(date)
        else:
            self.dateEdit_2.setDate(date)

        self.d.close()

    def showCalendarDialog(self, se):
        try:
            self.d = QDialog()

            self.d.resize(276, 247)
            self.calendarWidget = QtWidgets.QCalendarWidget(self.d)
            self.calendarWidget.setGeometry(QtCore.QRect(10, 10, 256, 183))
            self.calendarWidget.setObjectName("calendarWidget")
            self.buttonBox = QtWidgets.QDialogButtonBox(self.d)
            self.buttonBox.setGeometry(QtCore.QRect(10, 210, 156, 23))
            self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
            self.buttonBox.accepted.connect(functools.partial(self.acceptBtn, se))
            self.buttonBox.rejected.connect(self.d.reject)
            self.buttonBox.setObjectName("buttonBox")

            self.d.setWindowTitle("Employee Menu")
            self.d.setWindowIcon(QtGui.QIcon(r'C:\Users\DupuisF\Downloads\Report.ico'))
            self.d.exec_()

        except Exception as e:
            print(str(e))


class Dialog(QtWidgets.QDialog, Ui_Dialog):
    def __init__(self, parent=Ui_MainWindow):
        QtWidgets.QDialog.__init__(self, parent)
        self.setupUi(self, parent)
        self.setFields()

