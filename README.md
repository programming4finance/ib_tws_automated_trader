# Interactive Brokers TWS Automated Trader
> Uses Interactive Brokers Python API to implement algorithmic trading strategies on TWS (Trader Workstation)
---

### Table of Contents

- [Important Notes](#Important Notes)
- [Installation](#Installation)
- [Features](#features)
- [Contributing](#contributing)
- [Team](#team)
- [FAQ](#faq)
- [Support](#support)
- [License](#license)

# Important Notes

---
- We are not using the libraries IbPy and or IbPy2 in this tutorial. Those are separate API's for Interactive Brokers on-line trading system.
- We are using instructions directly from Interactive Brokers Website. **The official Trader Workstation Api docs are found here:**  <a href="https://interactivebrokers.github.io/tws-api/index.html" target="_blank">`https://interactivebrokers.github.io/tws-api/index.html`</a>

# Installation
---
### Installing the API Client

- Navigate to <a href="http://interactivebrokers.github.io/" target="_blank">`http://interactivebrokers.github.io/`</a>

- Hit agree and choose the version in accordance to the operating system your running. Make sure to choose the latest version instead of the stable release. The stable WILL NOT include the Python client.

- Go through the installer (if on Windows) and complete the setup. A few key points with the installer
    
    - Make sure the source is saved to your main disk (in my case on Windows, C drive). If a folder named TWS API isn't automatically created, make sure do create one. 

- After the install is finished enter the pythonclient directory and manually install the package. You can do this via the terminal. 
    
    - Change your directory to the pythonclient directory you on your disk
    
    > Note... you may want to verify which version of python you're using at this time by typing in - `python --version` in the terminal. The package will be installed to that runtime versions site-packages directory
    
    - Type in - `python setup.py` install. This will result in the creation of a site-package called ib which you can import once the install is finished.
    
    ![Recordit GIF](http://g.recordit.co/rYHAHs7bu8.gif)

    **Congrats, you've succesfully installed the interactive brokers API client for Python!**
    
### Installing TWS
- Download TWS from <a href="https://www.interactivebrokers.com/en/index.php?f=16040" target="_blank">`https://www.interactivebrokers.com/en/index.php?f=16040`</a>

> Note, we are using this as a PAPER TRADING account only. You'll have to create a complete non-demo account on Interactive Brokers to subscribe to market data and use all the features of the platform.

> I recommend creating an account as you don't need to maintain a minimum balance. If you want to do this later you can always launch TWS in demo mode.

- Once finished launch TWS (Trader Workstation) and login if you created an account

-In order to enable our python program to communicate with TWS, we need to enable ActiveX and socket clients. We can do this by navigating to the file menu option, global configuration, API, and then settings. Check off the Enable ActiveX and Socket Clients checkbox. Our script will communicate via a socket with TWS on port 7497

>If no Master API client ID exists, type in an arbitrary number

![Recordit GIF](http://g.recordit.co/OCxsNLINGc.gif)

- TWS will prevent us from sending out certain orders automatically. To bypass this navigate to the file menu option, global configuration, API, and then precautions. Click the first checkbox to bypass order precautions for API orders.

<br/><br/>

![Recordit GIF](http://g.recordit.co/OCxsNLINGc.gif)

**Congrats! You've successfully configured TWS for automated trading via Python.**

### Other Python Files (Optional)
This tutorial will require a wrapper class to receive data from Interactive Brokers. While you could in theory write one yourself to override the API methods, you can find one in the project files that should be suitable.

--- 

# Demo

### Clone
- Clone this repo to your local machine using `https://github.com/fvcproductions/SOMEREPO`