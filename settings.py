import logging
from logging.config import fileConfig
from logging.config import dictConfig
import os
from logging.handlers import TimedRotatingFileHandler
import sys

base_path = os.path.dirname(os.path.realpath(__file__))

FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")
LOG_FILE = os.path.join(base_path, 'Logs', 'ib-trader.txt')


def get_console_handler():
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    return console_handler


def get_file_handler():
    file_handler = TimedRotatingFileHandler(LOG_FILE, when='midnight')
    file_handler.setFormatter(FORMATTER)
    return file_handler


def truncate_log():
    with open(LOG_FILE, 'w'):
        pass


def get_logger(logger_name):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)  # better to have too much log than not enough
    logger.addHandler(get_console_handler())
    logger.addHandler(get_file_handler())
    # with this pattern, it's rarely necessary to propagate the error up to parent
    logger.propagate = False
    return logger


truncate_log()
logger = get_logger("IB_TRADER")
