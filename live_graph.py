from tkinter import *
from random import randint
from strategies import TradingStrategies
from tws_actions import TWSActions
# these two imports are important
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import time

import threading
import pandas as pd
import datetime as dt


continuePlotting = False

# Instantiate action class for TWS platform
actions = TWSActions()

# Instantiate strategies class
strategy = TradingStrategies()

# Get wrapper and socket client
callback, tws = actions.instantiate_client_socket()

# Connect to TWS
actions._connect(tws)


def change_state():
    global continuePlotting
    if continuePlotting == True:
        continuePlotting = False
    else:
        continuePlotting = True


def data_points():
    '''
    Data handler for live graph update
    :return:
    '''

    symbol = 'PM'

    data = actions.get_historical(tws, callback, symbol)
    signals = strategy.sma(data=data, short_length=5, long_length=10)

    signals = signals.dropna()

    # Convert index to datetime and set as index
    signals['date'] = pd.to_datetime(data['date'], format='%Y%m%d  %H:%M:%S')
    signals.set_index('date', inplace=True)

    # Create boolean mask to select between trading periods
    mask = (signals.index > dt.datetime(dt.date.today().year, dt.date.today().month, dt.date.today().day, 9, 30,
                                     0)) & (signals.index <= dt.datetime.now())
    signals = signals.loc[mask]


    return signals



def app():
    # initialise a window.
    root = Tk()
    root.config(background='white')
    root.geometry("1000x700")

    lab = Label(root, text="SMA STRATEGY", bg='white').pack()

    fig, ax = plt.subplots(figsize=(16, 9))

    ax.grid()

    graph = FigureCanvasTkAgg(fig, master=root)
    graph.get_tk_widget().pack(side="top", fill='both', expand=True)

    def plotter():
        while continuePlotting:
            ax.cla()
            ax.grid()
            data = data_points()
            print(len(data))

            ax.plot(data.index, data.nominal_price, label='Nominal Price')

            # Plot short
            ax.plot(data.long_sma.index, data.long_sma,
                    label='{}-days SMA'.format(5))

            # Plot long
            ax.plot(data.short_sma.index, data.short_sma,
                    label='{}-days SMA'.format(10))

            # Plot buy signals
            ax.plot(data.loc[data.positions == 1.0].index,
                    data.short_sma[data.positions == 1.0],
                    '^', markersize=10, color='g', label="BUY")

            # Plot sell signals
            ax.plot(data.loc[data.positions == -1.0].index,
                    data.short_sma[data.positions == -1.0],
                    'v', markersize=10, color='r', label="SELL")

            ax.legend(loc='best')
            ax.set_ylabel('Price in $')

            # Set x axis format
            hrFmt = mdates.DateFormatter('%H:%M')
            ax.xaxis.set_major_formatter(hrFmt)

            graph.draw()
            time.sleep(1)

    def gui_handler():
        change_state()
        threading.Thread(target=plotter).start()

    gui_handler()

    root.mainloop()


if __name__ == '__main__':
    app()