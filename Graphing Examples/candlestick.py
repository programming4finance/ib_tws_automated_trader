# import numpy as np
# import matplotlib
# import matplotlib.pyplot as plt
# # from matplotlib.finance import candlestick
# # from matplotlib.finance import volume_overlay3
# # finance module is no longer part of matplotlib
# # see: https://github.com/matplotlib/mpl_finance
# from mpl_finance import candlestick_ochl as candlestick
# from mpl_finance import volume_overlay3
# from matplotlib.dates import num2date
# from matplotlib.dates import date2num
# import matplotlib.mlab as mlab
# import datetime
# from strategies import TradingStrategies
# from tws_actions import TWSActions
#
# # datafile = 'data.csv'
# # r = mlab.csv2rec(datafile, delimiter=';')
#
# # Instantiate action class for TWS platform
# actions = TWSActions()
#
# # Instantiate strategies class
# strategy = TradingStrategies()
#
# # Get wrapper and socket client
# callback, tws = actions.instantiate_client_socket()
#
# # Connect to TWS
# actions._connect(tws)
#
# symbol = 'PM'
#
# data = actions.get_historical(tws, callback, symbol)
#
# # the dates in my example file-set are very sparse (and annoying) change the dates to be sequential
# for i in range(len(r)-1):
#     r['date'][i+1] = r['date'][i] + datetime.timedelta(days=1)
#
# candlesticks = zip(date2num(r['date']),r['open'],r['close'],r['high'],r['low'],r['volume'])
#
# fig = plt.figure()
# ax = fig.add_subplot(1,1,1)
#
# ax.set_ylabel('Quote ($)', size=20)
# candlestick(ax, candlesticks,width=1,colorup='g', colordown='r')
#
# # shift y-limits of the candlestick plot so that there is space at the bottom for the volume bar chart
# pad = 0.25
# yl = ax.get_ylim()
# ax.set_ylim(yl[0]-(yl[1]-yl[0])*pad,yl[1])
#
# # create the second axis for the volume bar-plot
# ax2 = ax.twinx()
#
#
# # set the position of ax2 so that it is short (y2=0.32) but otherwise the same size as ax
# ax2.set_position(matplotlib.transforms.Bbox([[0.125,0.1],[0.9,0.32]]))
#
# # get data from candlesticks for a bar plot
# dates = [x[0] for x in candlesticks]
# dates = np.asarray(dates)
# volume = [x[5] for x in candlesticks]
# volume = np.asarray(volume)
#
# # make bar plots and color differently depending on up/down for the day
# pos = r['open']-r['close']<0
# neg = r['open']-r['close']>0
# ax2.bar(dates[pos],volume[pos],color='green',width=1,align='center')
# ax2.bar(dates[neg],volume[neg],color='red',width=1,align='center')
#
# #scale the x-axis tight
# ax2.set_xlim(min(dates),max(dates))
# # the y-ticks for the bar were too dense, keep only every third one
# yticks = ax2.get_yticks()
# ax2.set_yticks(yticks[::3])
#
# ax2.yaxis.set_label_position("right")
# ax2.set_ylabel('Volume', size=20)
#
# # format the x-ticks with a human-readable date.
# xt = ax.get_xticks()
# new_xticks = [datetime.date.isoformat(num2date(d)) for d in xt]
# ax.set_xticklabels(new_xticks,rotation=45, horizontalalignment='right')
#
# plt.ion()
# plt.show()
#
# import matplotlib.pyplot as plt
# import matplotlib.dates as mdates
# import matplotlib.ticker as mticker
# from mpl_finance import candlestick_ohlc
# import numpy as np
# import urllib
# import datetime as dt
#
#
# def bytespdate2num(fmt, encoding='utf-8'):
#     strconverter = mdates.strpdate2num(fmt)
#     def bytesconverter(b):
#         s = b.decode(encoding)
#         return strconverter(s)
#     return bytesconverter
#
#
# def graph_data(stock):
#
#     fig = plt.figure()
#     ax1 = plt.subplot2grid((1,1), (0,0))
#
#     # Unfortunately, Yahoo's API is no longer available
#     # feel free to adapt the code to another source, or use this drop-in replacement.
#     stock_price_url = 'https://pythonprogramming.net/yahoo_finance_replacement'
#     source_code = urllib.request.urlopen(stock_price_url).read().decode()
#     stock_data = []
#     split_source = source_code.split('\n')
#     for line in split_source[1:]:
#         split_line = line.split(',')
#         if len(split_line) == 7:
#             if 'values' not in line and 'labels' not in line:
#                 stock_data.append(line)
#
#
#     date, closep, highp, lowp, openp, adj_closep, volume = np.loadtxt(stock_data,
#                                                           delimiter=',',
#                                                           unpack=True,
#                                                           converters={0: bytespdate2num('%Y-%m-%d')})
#
#     x = 0
#     y = len(date)
#     ohlc = []
#
#     while x < y:
#         append_me = date[x], openp[x], highp[x], lowp[x], closep[x], volume[x]
#         ohlc.append(append_me)
#         x+=1
#
#
#     candlestick_ohlc(ax1, ohlc, width=0.4, colorup='#77d879', colordown='#db3f3f')
#
#     for label in ax1.xaxis.get_ticklabels():
#         label.set_rotation(45)
#
#     ax1.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))
#     ax1.xaxis.set_major_locator(mticker.MaxNLocator(10))
#     ax1.grid(True)
#
#
#     plt.xlabel('Date')
#     plt.ylabel('Price')
#     plt.title(stock)
#     plt.legend()
#     plt.subplots_adjust(left=0.09, bottom=0.20, right=0.94, top=0.90, wspace=0.2, hspace=0)
#     plt.show()
#
#
# graph_data('EBAY')

from mpl_finance import *
import matplotlib.pyplot as plt


from strategies import TradingStrategies
from tws_actions import TWSActions

# Instantiate action class for TWS platform
actions = TWSActions()

# Instantiate strategies class
strategy = TradingStrategies()

# Get wrapper and socket client
callback, tws = actions.instantiate_client_socket()

# Connect to TWS
actions._connect(tws)

symbol = 'PM'

import pandas as pd

data = actions.get_historical(tws, callback, symbol)
data['date'] = pd.to_datetime(data['date'], format='%Y%m%d  %H:%M:%S')
data.set_index('date', inplace=True)

import datetime as dt

# Create boolean mask to select between trading periods
mask = (data.index > dt.datetime(dt.date.today().year, dt.date.today().month, dt.date.today().day, 9, 30,
                                    0)) & (data.index <= dt.datetime.now())
data = data.loc[mask]

# Create figure
fig = plt.figure()
ax1 = fig.add_subplot(111)
# Plot the candlestick
candles = candlestick2_ohlc(ax1, data['open'].tolist(), data['close'].tolist(), data['high'].tolist(), data['low'].tolist(),
                       width=1, colorup='g')

# Add a seconds axis for the volume overlay
ax2 = ax1.twinx()

# Plot the volume overlay
bc = volume_overlay(ax2, data['open'].tolist(), data['close'].tolist(), data['volume'].tolist(), colorup='g', alpha=0.5, width=1)
ax2.add_collection(bc)
plt.show()