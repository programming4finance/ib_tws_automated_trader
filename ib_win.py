# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Frank  Dupuis\Desktop\ib_trade_main_window.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from pandas_model import PandasModel
from settings import tws_creds


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1638, 828)
        self.main_window = MainWindow
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        MainWindow.setCentralWidget(self.centralwidget)
        self.retranslateUi(MainWindow)
        self.portfolioInfoTabWidget.setCurrentIndex(1)
        # self.graphGroupBox.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate

        self.createStrategyWidget()
        self.createMenuBar()
        self.createPortfolioWidget()
        self.createDataConfigWidget()
        self.createOrderConfigWidget()
        # self.createGraphWidget()
        self.createPortolioBannerWidget()
        self.createPortfolioConstraintsWidget()

        MainWindow.setWindowTitle(_translate("MainWindow", "IB Automated Trader"))

        self.strategySelectGroupBox.setTitle(_translate("MainWindow", "Strategy Selector"))
        self.portfolioInfoTabWidget.setTabText(self.portfolioInfoTabWidget.indexOf(self.portfolio_tab),
                                               _translate("MainWindow", "Portfolio"))
        self.portfolioInfoTabWidget.setTabText(self.portfolioInfoTabWidget.indexOf(self.order_tab),
                                               _translate("MainWindow", "Orders"))
        self.dataConfigGroupBox.setTitle(_translate("MainWindow", "Data Configurations"))
        self.durationSize_label.setText(_translate("MainWindow", "Duration:"))
        self.barSize_label.setText(_translate("MainWindow", "Bar Size:"))
        self.orderConfigGroupBox.setTitle(_translate("MainWindow", "Order Configurations"))
        self.orderTypeLabel.setText(_translate("MainWindow", "Order Type:"))
        self.symbolLabel.setText(_translate("MainWindow", "Symbol:"))
        self.shareQtyLabel.setText(_translate("MainWindow", "Share Quantity:"))
        self.order_submit_btn.setText(_translate("MainWindow", "Submit"))
        self.portfolioBannerGroupBox.setTitle(_translate("MainWindow", "Portfolio Summary"))
        self.portfolioConstraintsGroupBox.setTitle(_translate("MainWindow", "Portfolio Constraints"))

        # self.graphGroupBox.setTabText(self.graphGroupBox.indexOf(self.position_tab),
        #                               _translate("MainWindow", "Position"))
        # self.graphGroupBox.setTabText(self.graphGroupBox.indexOf(self.market_tab), _translate("MainWindow", "Market"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuEdit.setTitle(_translate("MainWindow", "Edit"))

    def createPortolioBannerWidget(self):
        self.portfolioBannerGroupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.portfolioBannerGroupBox.setGeometry(QtCore.QRect(10, 10, 650, 111))
        self.portfolioBannerGroupBox.setObjectName("portfolioBannerGroupBox")
        self.portfolioBannerVerticalLayoutWidget = QtWidgets.QWidget(self.portfolioBannerGroupBox)
        self.portfolioBannerVerticalLayoutWidget.setGeometry(QtCore.QRect(10, 20, 630, 80))
        self.portfolioBannerVerticalLayoutWidget.setObjectName("portfolioBannerVerticalLayoutWidget")
        self.portfolioBannerVerticalLayout = QtWidgets.QVBoxLayout(self.portfolioBannerVerticalLayoutWidget)
        self.portfolioBannerVerticalLayout.setContentsMargins(0, 0, 0, 0)
        self.portfolioBannerVerticalLayout.setObjectName("portfolioBannerVerticalLayout")
        self.portfolioBannerTableView = QtWidgets.QTableView(self.portfolioBannerVerticalLayoutWidget)
        self.portfolioBannerTableView.setObjectName("portfolioBannerTableView")
        self.portfolioBannerTableView.setAlternatingRowColors(True)
        self.portfolioBannerVerticalLayout.addWidget(self.portfolioBannerTableView)

        # from tws_actions import TWSActions
        # # Instantiate action class for TWS platform
        # actions = TWSActions()
        # # Get wrapper and socket client
        # callback, tws = actions.instantiate_client_socket()
        # # Connect to TWS
        # actions._connect(tws)
        # portfolio, acct = actions.get_account_summary(tws, callback, tws_creds['name'])
        #
        # acct = acct.T # transpose data frame
        # header = acct.iloc[0]  # grab the first row for the header
        # acct = acct.iloc[[1]]# take the data less the header row
        # acct.columns = header  # set the header row as the df header
        #
        # acct = acct[["AvailableFunds", "BuyingPower", "ExcessLiquidity", "GrossPositionValue", "UnrealizedPnL"]]
        # acct = acct.loc[:, ~acct.columns.duplicated()]
        #
        # actions._disconnect(tws)
        #
        # model = PandasModel(acct)
        #
        # # Set pandas model
        # self.portfolioBannerTableView.setModel(model)
        # # Hide index column
        # self.portfolioBannerTableView.verticalHeader().setVisible(False)

    def createPortfolioConstraintsWidget(self):
        self.portfolioConstraintsGroupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.portfolioConstraintsGroupBox.setGeometry(QtCore.QRect(800, 10, 650, 111))
        self.portfolioConstraintsGroupBox.setObjectName("portfolioConstraintsGroupBox")
        self.portfolioConstraintsVerticalLayoutWidget = QtWidgets.QWidget(self.portfolioConstraintsGroupBox)
        self.portfolioConstraintsVerticalLayoutWidget.setGeometry(QtCore.QRect(400, 20, 630, 80))
        self.portfolioConstraintsVerticalLayoutWidget.setObjectName("portfolioConstraintsVerticalLayoutWidget")
        self.portfolioConstraintsVerticalLayout = QtWidgets.QVBoxLayout(self.portfolioConstraintsVerticalLayoutWidget)
        self.portfolioConstraintsVerticalLayout.setContentsMargins(0, 0, 0, 0)
        self.portfolioConstraintsVerticalLayout.setObjectName("portfolioConstraintsVerticalLayout")
        self.portfolioConstraintsTableView = QtWidgets.QTableView(self.portfolioConstraintsVerticalLayoutWidget)
        self.portfolioConstraintsTableView.setObjectName("portfolioConstraintsTableView")
        self.portfolioConstraintsTableView.setAlternatingRowColors(True)
        self.portfolioConstraintsVerticalLayout.addWidget(self.portfolioConstraintsTableView)

    # def createGraphWidget(self):
    #     self.graphGroupBox = QtWidgets.QTabWidget(self.centralwidget)
    #     self.graphGroupBox.setGeometry(QtCore.QRect(790, 10, 761, 361))
    #     self.graphGroupBox.setObjectName("graphGroupBox")
    #     self.position_tab = QtWidgets.QWidget()
    #     self.position_tab.setObjectName("position_tab")
    #     self.positionVerticalLayoutWidget = QtWidgets.QWidget(self.position_tab)
    #     self.positionVerticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 741, 311))
    #     self.positionVerticalLayoutWidget.setObjectName("positionVerticalLayoutWidget")
    #     self.positionVlayout = QtWidgets.QVBoxLayout(self.positionVerticalLayoutWidget)
    #     self.positionVlayout.setContentsMargins(0, 0, 0, 0)
    #     self.positionVlayout.setObjectName("positionVlayout")
    #     self.graphGroupBox.addTab(self.position_tab, "")
    #     self.market_tab = QtWidgets.QWidget()
    #     self.market_tab.setObjectName("market_tab")
    #     self.marketVerticalLayoutWidget = QtWidgets.QWidget(self.market_tab)
    #     self.marketVerticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 741, 311))
    #     self.marketVerticalLayoutWidget.setObjectName("marketVerticalLayoutWidget")
    #     self.marketVerticalLayout = QtWidgets.QVBoxLayout(self.marketVerticalLayoutWidget)
    #     self.marketVerticalLayout.setContentsMargins(0, 0, 0, 0)
    #     self.marketVerticalLayout.setObjectName("marketVerticalLayout")
    #     self.graphGroupBox.addTab(self.market_tab, "")

    def createOrderConfigWidget(self):
        self.orderConfigGroupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.orderConfigGroupBox.setGeometry(QtCore.QRect(220, 140, 291, 171))
        self.orderConfigGroupBox.setObjectName("orderConfigGroupBox")
        self.orderFormLayoutWidget = QtWidgets.QWidget(self.orderConfigGroupBox)
        self.orderFormLayoutWidget.setGeometry(QtCore.QRect(10, 30, 271, 131))
        self.orderFormLayoutWidget.setObjectName("orderFormLayoutWidget")
        self.orderFormLayout = QtWidgets.QFormLayout(self.orderFormLayoutWidget)
        self.orderFormLayout.setContentsMargins(0, 0, 0, 0)
        self.orderFormLayout.setObjectName("orderFormLayout")
        self.orderTypeLabel = QtWidgets.QLabel(self.orderFormLayoutWidget)
        self.orderTypeLabel.setObjectName("orderTypeLabel")
        self.orderFormLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.orderTypeLabel)
        self.orderTypeComboBox = QtWidgets.QComboBox(self.orderFormLayoutWidget)
        self.orderTypeComboBox.setObjectName("orderTypeComboBox")
        self.orderFormLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.orderTypeComboBox)
        self.symbolLabel = QtWidgets.QLabel(self.orderFormLayoutWidget)
        self.symbolLabel.setObjectName("symbolLabel")
        self.orderFormLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.symbolLabel)
        self.symbolTextBox = QtWidgets.QTextEdit(self.orderFormLayoutWidget)
        self.symbolTextBox.setObjectName("symbolTextBox")
        self.orderFormLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.symbolTextBox)
        self.shareQtyTextBox = QtWidgets.QTextEdit(self.orderFormLayoutWidget)
        self.shareQtyTextBox.setObjectName("shareQtyTextBox")
        self.orderFormLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.shareQtyTextBox)
        self.shareQtyLabel = QtWidgets.QLabel(self.orderFormLayoutWidget)
        self.shareQtyLabel.setObjectName("shareQtyLabel")
        self.orderFormLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.shareQtyLabel)
        self.order_submit_btn = QtWidgets.QPushButton(self.orderFormLayoutWidget)
        self.order_submit_btn.setObjectName("order_submit_btn")
        self.orderFormLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.order_submit_btn)

        # Add items to combobox and set text
        self.orderTypeComboBox.addItem("")
        self.orderTypeComboBox.addItem("")
        self.orderTypeComboBox.addItem("")

        self.orderTypeComboBox.setItemText(0, "")
        self.orderTypeComboBox.setItemText(1, "MKT")
        self.orderTypeComboBox.setItemText(2, "LMT")

    def createDataConfigWidget(self):
        self.dataConfigGroupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.dataConfigGroupBox.setGeometry(QtCore.QRect(20, 210, 171, 91))
        self.dataConfigGroupBox.setObjectName("dataConfigGroupBox")
        self.dataFormLayoutWidget = QtWidgets.QWidget(self.dataConfigGroupBox)
        self.dataFormLayoutWidget.setGeometry(QtCore.QRect(10, 20, 151, 61))
        self.dataFormLayoutWidget.setObjectName("dataFormLayoutWidget")
        self.dataFormLayout = QtWidgets.QFormLayout(self.dataFormLayoutWidget)
        self.dataFormLayout.setContentsMargins(0, 0, 0, 0)
        self.dataFormLayout.setObjectName("dataFormLayout")
        self.durationSize_label = QtWidgets.QLabel(self.dataFormLayoutWidget)
        self.durationSize_label.setObjectName("durationSize_label")
        self.dataFormLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.durationSize_label)

        # Create combobox widget
        self.durationComboBox = QtWidgets.QComboBox(self.dataFormLayoutWidget)
        self.durationComboBox.setObjectName("durationComboBox")

        # Add items to combobox and set text
        self.durationComboBox.addItem("")
        self.durationComboBox.addItem("")
        self.durationComboBox.addItem("")
        self.durationComboBox.addItem("")
        self.durationComboBox.addItem("")

        self.durationComboBox.setItemText(0, "")
        self.durationComboBox.setItemText(1, "D")
        self.durationComboBox.setItemText(2, "W")
        self.durationComboBox.setItemText(3, "M")
        self.durationComboBox.setItemText(4, "Y")

        # Add widget to layout
        self.dataFormLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.durationComboBox)

        self.barSize_label = QtWidgets.QLabel(self.dataFormLayoutWidget)
        self.barSize_label.setObjectName("barSize_label")

        # create combobox widget
        self.dataFormLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.barSize_label)
        self.barSizeComboBox = QtWidgets.QComboBox(self.dataFormLayoutWidget)
        self.barSizeComboBox.setObjectName("barSizeComboBox")

        # Add items
        self.barSizeComboBox.addItem("")
        self.barSizeComboBox.addItem("")
        self.barSizeComboBox.addItem("")
        self.barSizeComboBox.addItem("")
        self.barSizeComboBox.addItem("")
        self.barSizeComboBox.addItem("")
        self.barSizeComboBox.addItem("")
        self.barSizeComboBox.addItem("")
        self.barSizeComboBox.addItem("")
        self.barSizeComboBox.addItem("")
        self.barSizeComboBox.addItem("")
        self.barSizeComboBox.addItem("")

        # Set item text
        self.barSizeComboBox.setItemText(0, "")
        self.barSizeComboBox.setItemText(1, "1 min")
        self.barSizeComboBox.setItemText(2, "2 mins")
        self.barSizeComboBox.setItemText(3, "3 mins")
        self.barSizeComboBox.setItemText(4, "5 mins")
        self.barSizeComboBox.setItemText(5, "10 mins")
        self.barSizeComboBox.setItemText(6, "15 mins")
        self.barSizeComboBox.setItemText(7, "20 mins")
        self.barSizeComboBox.setItemText(8, "30 mins")
        self.barSizeComboBox.setItemText(9, "1 hour")
        self.barSizeComboBox.setItemText(10, "2 hours")
        self.barSizeComboBox.setItemText(11, "3 hours")
        self.barSizeComboBox.setItemText(12, "4 hours")
        self.barSizeComboBox.setItemText(13, "8 hours")
        self.barSizeComboBox.setItemText(14, "1 day")
        self.barSizeComboBox.setItemText(15, "1 week")
        self.barSizeComboBox.setItemText(16, "1 month")

        # Add to layout
        self.dataFormLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.barSizeComboBox)

    def createPortfolioWidget(self):
        self.portfolioInfoTabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.portfolioInfoTabWidget.setGeometry(QtCore.QRect(10, 400, 761, 371))
        self.portfolioInfoTabWidget.setObjectName("portfolioInfoTabWidget")
        self.portfolio_tab = QtWidgets.QWidget()
        self.portfolio_tab.setObjectName("portfolio_tab")
        self.portfolioTableView = QtWidgets.QTableView(self.portfolio_tab)
        self.portfolioTableView.setGeometry(QtCore.QRect(10, 10, 841, 321))
        self.portfolioTableView.setObjectName("portfolioTableView")
        self.portfolioInfoTabWidget.addTab(self.portfolio_tab, "")
        self.order_tab = QtWidgets.QWidget()
        self.order_tab.setObjectName("order_tab")
        self.orderTableView = QtWidgets.QTableView(self.order_tab)
        self.orderTableView.setGeometry(QtCore.QRect(10, 10, 731, 331))
        self.orderTableView.setObjectName("orderTableView")
        self.portfolioInfoTabWidget.addTab(self.order_tab, "")

    def createMenuBar(self):
        self.menubar = QtWidgets.QMenuBar(self.main_window)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1638, 21))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuEdit = QtWidgets.QMenu(self.menubar)
        self.menuEdit.setObjectName("menuEdit")
        self.main_window.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(self.main_window)
        self.statusbar.setObjectName("statusbar")
        self.main_window.setStatusBar(self.statusbar)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())

    def createStrategyWidget(self):
        '''
        Function to create strategy selector combobox
        :return: None
        '''

        # Encase in group box
        self.strategySelectGroupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.strategySelectGroupBox.setGeometry(QtCore.QRect(20, 140, 191, 51))

        # Instantiate combobox object and set dimensions
        self.strategySelectGroupBox.setObjectName("strategySelectGroupBox")
        self.strategyComboBox_3 = QtWidgets.QComboBox(self.strategySelectGroupBox)
        self.strategyComboBox_3.setGeometry(QtCore.QRect(10, 20, 171, 21))
        self.strategyComboBox_3.setObjectName("strategyComboBox_3")

        # Add items to combobox and set text
        self.strategyComboBox_3.addItem("")
        self.strategyComboBox_3.addItem("")
        self.strategyComboBox_3.addItem("")
        self.strategyComboBox_3.addItem("")
        self.strategyComboBox_3.setItemText(0, "")
        self.strategyComboBox_3.setItemText(1, "SMA")
        self.strategyComboBox_3.setItemText(2, "PSAR")
        self.strategyComboBox_3.setItemText(3, "MACD")
