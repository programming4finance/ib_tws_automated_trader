class TradingStrategies:
    def __init__(self):
        pass

    def SMA(self, data, short_length, long_length):
        '''

        :param data:
        :param short_length:
        :param long_length:
        :return:
        '''

        # calculate two moving averages
        data = data.dropna()
        short_SMA = data['close'].rolling(short_length).mean()
        long_SMA = data['close'].rolling(long_length).mean()

        return short_SMA.iloc[-1], long_SMA.iloc[-1]

    def check_existing_pos(self):
        # 0 is no position, 1 means we have existing position
        # we need to write an algo to send request to IB to check
        # whether we have existing position
        # you will need a proper identifier for your position.
        pos = 1
        return pos
